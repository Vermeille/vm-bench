# README #

### On VM implementations ###

VMs can be implemented a number of ways. This project just sets a playground
with a toy bytecode and a toy example so that you can compare performance.

### How do I get set up? ###

You only need gcc and make. Those tools are often provided on linux in a
`base-devel` package.

Just run `make bench` to compile it all and print the results on the different
implementations.

### What's in? ###

For now, the benchmark test is really stupid: it sums integer from 1 to 100000.
But it's okay for a rough estimation. Of course, it'll go better. As long as
I'm not bored writing benchmarks.

We now have 3 VM:

* A naïve switch/case bytecode interpreter;
* A better goto interpreter;
* A manually written assembly interpreter.
