#include <stdlib.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>

enum Opcode {
#define X(Mnemo, Opcode, Arg) Mnemo = Opcode,
#include "opcodes.def"
#undef X
};

int main(int argc, char** argv)
{
    int fd = open(argv[1], O_RDONLY);
    if (fd == -1)
    {
        fprintf(stderr, "bad file descriptor\n");
        return 1;
    }
    struct stat s;
    fstat(fd, &s);
    const unsigned char* page = mmap(NULL, s.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    int pc = 0;
    int* stack = malloc(1024*2014);
    int vars[16];
    int sp = 0;

    const void* op_addr[] = {
#define X(Mnemo, Opcode, Arg) [Opcode] = &&Mnemo,
#include "opcodes.def"
#undef X
    };

    goto *op_addr[page[pc]];

LDI:
    ++pc;
    stack[sp] = *(int*)(&page[pc]);
    ++sp;
    pc += 4;
    goto *op_addr[page[pc]];
STOP:
    exit(0);
LD:
    ++pc;
    stack[sp++] = vars[page[pc++]];
    goto *op_addr[page[pc]];
DUP:
    ++pc;
    stack[sp] = stack[sp - 1];
    ++sp;
    goto *op_addr[page[pc]];
ST:
    ++pc;
    vars[page[pc++]] = stack[--sp];
    goto *op_addr[page[pc]];
ADD:
    --sp;
    ++pc;
    stack[sp - 1] += stack[sp];
    goto *op_addr[page[pc]];
SUB:
    --sp;
    stack[sp - 1] -= stack[sp];
    ++pc;
    goto *op_addr[page[pc]];
EQ:
    stack[sp - 2] = stack[sp - 1] == stack[sp - 2];
    --sp;
    ++pc;
    goto *op_addr[page[pc]];
NE:
    stack[sp - 2] = stack[sp - 1] != stack[sp - 2];
    --sp;
    ++pc;
    goto *op_addr[page[pc]];
GT:
    stack[sp - 2] = stack[sp - 1] > stack[sp - 2];
    --sp;
    ++pc;
    goto *op_addr[page[pc]];
JMP:
    ++pc;
    pc = *(int*)&page[pc];
    goto *op_addr[page[pc]];
JMPZ:
    --sp;
    ++pc;
    if (stack[sp - 1])
        pc = *(int*)&page[pc];
    else
        pc += 4;
    goto *op_addr[page[pc]];
READ:
    scanf("%d", &stack[sp++]);
    ++pc;
    goto *op_addr[page[pc]];
WRITE:
    ++pc;
    printf("%d\n", stack[sp - 1]);
    goto *op_addr[page[pc]];
DBG:
    ++pc;
    for (int i = 0; i < 16; ++i)
        printf("%d ", vars[i]);
    printf("]\n");
    goto *op_addr[page[pc]];
    return 0;
}
