#include <stdlib.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>

#define X(Mnemo, Op, Arg) void Mnemo(void);
#include "opcodes.def"
#undef X

int vars[16];

const void* dispatch[] = {
#define X(Mnemo, Op, Arg) [Op] = &Mnemo,
#include "opcodes.def"
#undef X
};

void execute(const void*);

int main(int argc, char** argv)
{
    int fd = open(argv[1], O_RDONLY);
    if (fd == -1)
    {
        fprintf(stderr, "bad file descriptor\n");
        return 1;
    }
    struct stat s;
    fstat(fd, &s);
    const unsigned char* page = mmap(NULL, s.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    printf("%p\n", (void*)page);

    execute(page);

    return 0;
}
