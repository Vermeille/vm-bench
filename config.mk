CC=clang
CFLAGS=-Wall -Wextra -std=c99 -O3 -I../common

CXX=clang++
CXXFLAGS=-Wall -Wextra -std=c++11 -O3 -I../common

all: $(BIN)

clean:
	rm -f *.o $(BIN)

bench: $(BIN)
	make bench-sample -C ..
	time ./$(BIN) ../common/bench-sample
