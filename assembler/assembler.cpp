#include <map>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

enum Opcode {
#define X(Mnemo, Opcode, Arg) Mnemo = Opcode,
#include "opcodes.def"
#undef X
};

struct Instr
{
    Opcode op;
    int iarg;
    std::string sarg;
};

int main(int argc, char** argv)
{
    std::ifstream ifs(argv[1]);
    std::vector<Instr> instrs;
    std::map<std::string, int> labels;

    std::string line;

    int size_so_far = 0;

    while (std::getline(ifs, line))
    {
        if (line == "")
            continue;

        std::istringstream iss(line);
        std::string token;
        iss >> token;

        Instr i;

        if (token.back() == ':')
        {
            labels[token] = size_so_far;
            continue;
        }

        if (0) {}
#define X(Mnemo, Opcode, Arg)                   \
        else if (token == #Mnemo)               \
        {                                       \
            i.op = Mnemo;                       \
            if (Arg == 'i')                     \
            {                                   \
                size_so_far += 5;               \
                iss >> i.iarg;                  \
            }                                   \
            else if (Arg == 'b')                \
            {                                   \
                iss >> i.iarg;                  \
                size_so_far += 2;               \
            }                                   \
            else if (Arg == 's')                \
            {                                   \
                iss >> i.sarg;                  \
                size_so_far += 5;               \
            }                                   \
            else if (Arg == 'v')                \
            {                                   \
                ++size_so_far;                  \
            }                                   \
        }
#include "opcodes.def"
#undef X
        else if (token != "")
        {
            std::cerr << "unkown opcode " << token << std::endl;
            return 1;
        }
        instrs.push_back(i);
    }

    std::ofstream ofs(argv[2]);

    for (auto& i : instrs)
    {
        ofs.write(reinterpret_cast<char*>(&i.op), 1);
        switch (i.op)
        {
            case LDI:
                ofs.write(reinterpret_cast<char*>(&i.iarg), 4);
                break;
            case LD:
            case ST:
                ofs.write(reinterpret_cast<char*>(&i.iarg), 1);
                break;
            case JMP:
            case JMPZ:
                ofs.write(reinterpret_cast<char*>(&labels[i.sarg]), 4);
                break;
            default:
                break;
        }
    }
    ofs.close();

    return 0;
}
