VMs = asm better_interpreter interpreter
PROJECTS := assembler $(VMs)

all:
	for p in $(PROJECTS); do make all -C $$p; done

assembler/assembler:
	make all -C assembler

bench-sample: common/bench-sample

common/bench-sample: assembler/assembler
	./assembler/assembler common/test.asm $@

bench: all bench-sample
	for vm in $(VMs); do \
	    make bench -C $$vm; \
	done;

clean:
	for p in $(PROJECTS); do make clean -C $$p; done
	rm -f common/bench-sample
