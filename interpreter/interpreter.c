#include <stdlib.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

enum Opcode {
#define X(Mnemo, Opcode, Arg) Mnemo = Opcode,
#include "opcodes.def"
#undef X
};

#ifdef DEBUG
void disas(const unsigned char* mem, int pc)
{
#define X(Mnemo, Op, Arg) \
    if (mem[pc] == Op) \
    {                       \
        printf(#Mnemo);     \
        if (Arg == 'b')     \
            printf("%d", mem[pc + 1]); \
        if (Arg == 'i')     \
            printf("%d", *(int*)&mem[pc + 1]); \
    }
#include "opcodes.def"
#undef X
    printf("\n");
}
#endif

int main(int argc, char** argv)
{
    int fd = open(argv[1], O_RDONLY);
    struct stat s;
    fstat(fd, &s);
    const unsigned char* page = mmap(NULL, s.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    int pc = 0;
    int* stack = malloc(1024*2014);
    int vars[16];
    int sp = 0;

    while (1)
    {
#ifdef DEBUG
        char cmd[255];
        printf("debug> ");
        scanf("%s", cmd);
        if (!strcmp(cmd, "stack"))
        {
            printf("stack: [ ");
            for (int i = 0; i < sp; ++i)
                printf("%d ", stack[i]);
            printf("]\n");
        }
        else if (!strcmp(cmd, "vars"))
        {
            printf("vars: [ ");
            for (int i = 0; i < 16; ++i)
                printf("%d ", vars[i]);
            printf("]\n");
        }
        else if (!strcmp(cmd, "go"))
        {}
        disas(page, pc);
#endif
        switch (page[pc++])
        {
            case LDI:
                stack[sp] = *(int*)(&page[pc]);
                ++sp;
                pc += 4;
                break;
            case STOP:
                exit(0);
            case LD:
                stack[sp++] = vars[page[pc++]];
                break;
            case DUP:
                stack[sp] = stack[sp - 1];
                ++sp;
                break;
            case ST:
                vars[page[pc++]] = stack[--sp];
                break;
            case ADD:
                --sp;
                stack[sp - 1] += stack[sp];
                break;
            case SUB:
                --sp;
                stack[sp - 1] -= stack[sp];
                break;
            case EQ:
                stack[sp - 2] = stack[sp - 1] == stack[sp - 2];
                --sp;
                break;
            case NE:
                stack[sp - 2] = stack[sp - 1] != stack[sp - 2];
                --sp;
                break;
            case GT:
                stack[sp - 2] = stack[sp - 1] > stack[sp - 2];
                --sp;
                break;
            case JMP:
                pc = *(int*)&page[pc];
                break;
            case JMPZ:
                --sp;
                if (stack[sp - 1])
                    pc = *(int*)&page[pc];
                else
                    pc += 4;
                break;
            case READ:
                scanf("%d", &stack[sp++]);
                break;
            case WRITE:
                printf("%d\n", stack[sp - 1]);
                break;
            case DBG:
                for (int i = 0; i < 16; ++i)
                    printf("%d ", vars[i]);
                printf("]\n");
                break;
        }
    }
    return 0;
}
